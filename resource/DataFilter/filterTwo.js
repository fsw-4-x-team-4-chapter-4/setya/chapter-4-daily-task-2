const data = require("../data.js");
const errorHandler = require("../errorHandler.js");

function filterTwo (data) {
// Tempat penampungan hasil
const result = [];

data.forEach((data) => {
    if ((data.gender === "female" || data.company === "FSW4") && data.age > 30){
        result.push(data);
    }
    
})

return (result.length < 1) ? errorHandler() : result;
}

module.exports = filterTwo(data)
